import org.gradle.api.internal.FeaturePreviews

enableFeaturePreview(FeaturePreviews.Feature.IMPROVED_POM_SUPPORT.name)

rootProject.name = "protobuf→avro→parquet→avro→protobuf"
